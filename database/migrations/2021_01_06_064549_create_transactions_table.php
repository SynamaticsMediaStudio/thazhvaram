<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('customer_id');
            $table->string('booking_id');
            $table->string('type')->default('PAYMENT');
            $table->float('gross')->default(0);
            $table->float('gst')->default(0);
            $table->float('gst_percentage')->default(0);
            $table->float('net')->default(0);
            $table->string('method')->default("CASH");
            $table->string('status')->default("PAID");
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
