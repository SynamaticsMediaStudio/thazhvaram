<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('customer_id');
            $table->string('cottage_type_id');
            $table->string('cottage_id')->nullable();
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->datetime('checked_in_date')->nullable();
            $table->datetime('checked_out_date')->nullable();
            $table->float('gross')->default(0);
            $table->float('gst')->default(0);
            $table->float('gst_percentage')->default(0);
            $table->float('discount')->default(0);
            $table->float('net')->default(0);
            $table->string('status')->default('BOOKED');
            $table->longText('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
