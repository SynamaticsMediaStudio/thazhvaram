<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCottageTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cottage_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('rooms');
            $table->integer('max_children')->default(0);
            $table->integer('max_adults')->default(1);
            $table->float('share_percentage')->default(0);
            $table->float('cost')->default(0);
            $table->float('gst')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cottage_types');
    }
}
