<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cottage extends Model
{
    protected $fillable = ['building_id','cottage_type_id'];
    public function cottage_type()
    {
        return $this->belongsTo("App\CottageType");
    }
}
