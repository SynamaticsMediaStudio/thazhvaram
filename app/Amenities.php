<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    protected $fillable  = ['name','description','icon'];
    public function cottage_types()
    {
        return $this->belongsToMany("App\CottageType",'cottage_type_amenities');
    }    
}
