<?php

namespace App\Http\Controllers;

use App\CustomerKyc;
use Illuminate\Http\Request;

class CustomerKycController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerKyc  $customerKyc
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerKyc $customerKyc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerKyc  $customerKyc
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerKyc $customerKyc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerKyc  $customerKyc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerKyc $customerKyc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerKyc  $customerKyc
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerKyc $customerKyc)
    {
        //
    }
}
