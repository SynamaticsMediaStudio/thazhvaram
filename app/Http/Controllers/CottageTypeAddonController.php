<?php

namespace App\Http\Controllers;

use App\CottageTypeAddon;
use Illuminate\Http\Request;

class CottageTypeAddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CottageTypeAddon  $cottageTypeAddon
     * @return \Illuminate\Http\Response
     */
    public function show(CottageTypeAddon $cottageTypeAddon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CottageTypeAddon  $cottageTypeAddon
     * @return \Illuminate\Http\Response
     */
    public function edit(CottageTypeAddon $cottageTypeAddon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CottageTypeAddon  $cottageTypeAddon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CottageTypeAddon $cottageTypeAddon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CottageTypeAddon  $cottageTypeAddon
     * @return \Illuminate\Http\Response
     */
    public function destroy(CottageTypeAddon $cottageTypeAddon)
    {
        //
    }
}
