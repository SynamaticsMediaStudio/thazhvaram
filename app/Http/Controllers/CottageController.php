<?php

namespace App\Http\Controllers;

use App\Cottage;
use Illuminate\Http\Request;

class CottageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cottages = Cottage::paginate();
        return view('cottages.index',['cottages'=>$cottages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cottageTypes = \App\CottageType::all();
        return view('cottages.create',['cottageTypes'=>$cottageTypes]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "building_id"=>"required|string|unique:cottages",
            "cottage_type_id" => "required|string|exists:cottage_types,id",
        ]);
        $cottage = Cottage::create([
            'building_id'  => $request->building_id,
            'cottage_type_id' => $request->cottage_type_id,         
        ]);
        if($cottage){
            return redirect()->route('cottages.index')->with(['success'=>__('cottages.messages.cottage_created')]);
        }
        else{
            return redirect()->back()->with(['error'=>__('cottages.messages.unable_to_create_cottage')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cottage  $cottage
     * @return \Illuminate\Http\Response
     */
    public function show(Cottage $cottage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cottage  $cottage
     * @return \Illuminate\Http\Response
     */
    public function edit(Cottage $cottage)
    {
        $cottageTypes = \App\CottageType::all();
        return view('cottages.edit',['cottageTypes'=>$cottageTypes,'cottage'=>$cottage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cottage  $cottage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cottage $cottage)
    {
        $request->validate([
            "building_id"=>"required|string|unique:cottages,building_id,".$cottage->id,
            "cottage_type_id" => "required|string|exists:cottage_types,id",
        ]);
        $cottage->update([
            'building_id'  => $request->building_id,
            'cottage_type_id' => $request->cottage_type_id,
        ]);
        if($cottage){
            return redirect()->route('cottages.index')->with(['success'=>__('cottages.messages.cottage_updated')]);
        }
        else{
            return redirect()->back()->with(['error'=>__('cottages.messages.unable_to_update_cottage')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cottage  $cottage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cottage $cottage)
    {
        //
    }
}
