<?php

namespace App\Http\Controllers;

use App\BookingAddon;
use Illuminate\Http\Request;

class BookingAddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookingAddon  $bookingAddon
     * @return \Illuminate\Http\Response
     */
    public function show(BookingAddon $bookingAddon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookingAddon  $bookingAddon
     * @return \Illuminate\Http\Response
     */
    public function edit(BookingAddon $bookingAddon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookingAddon  $bookingAddon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookingAddon $bookingAddon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookingAddon  $bookingAddon
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookingAddon $bookingAddon)
    {
        //
    }
}
