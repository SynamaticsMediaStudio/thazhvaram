<?php

namespace App\Http\Controllers;

use App\Amenities;
use Illuminate\Http\Request;

class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amenities = Amenities::paginate();
        return view('cottages.amenities.index',['amenities'=>$amenities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cottages.amenities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required|string|max:255",
            "description" => "nullable|string",
        ]);
        $amenity = Amenities::create([
            'name'  => $request->name,
            'description' => $request->description,
        ]);
        if($amenity){
            return redirect()->route('amenities.index')->with(['success'=>__('general.messages.created',['type'=>"Amenity"])]);
        }
        else{
            return redirect()->back()->with(['error'=>__('general.messages.unable_to_create',['type'=>"Amenity"])]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Amenities  $amenities
     * @return \Illuminate\Http\Response
     */
    public function show(Amenities $amenity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Amenities  $amenities
     * @return \Illuminate\Http\Response
     */
    public function edit(Amenities $amenity)
    {
        return view('cottages.amenities.edit',['amenity'=>$amenity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Amenities  $amenities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Amenities $amenity)
    {
        $request->validate([
            "name"=>"required|string|max:255",
            "description" => "nullable|string",
        ]);
        $amenity->update([
            'name'  => $request->name,
            'description' => $request->description,
        ]);
        if($amenity){
            return redirect()->route('amenities.index')->with(['success'=>__('general.messages.updated',['type'=>"Amenity"])]);
        }
        else{
            return redirect()->back()->with(['error'=>__('general.messages.unable_to_update',['type'=>"Amenity"])]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Amenities  $amenities
     * @return \Illuminate\Http\Response
     */
    public function destroy(Amenities $amenity)
    {
        //
    }
}
