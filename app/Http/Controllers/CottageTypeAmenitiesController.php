<?php

namespace App\Http\Controllers;

use App\CottageTypeAmenities;
use Illuminate\Http\Request;

class CottageTypeAmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CottageTypeAmenities  $cottageTypeAmenities
     * @return \Illuminate\Http\Response
     */
    public function show(CottageTypeAmenities $cottageTypeAmenities)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CottageTypeAmenities  $cottageTypeAmenities
     * @return \Illuminate\Http\Response
     */
    public function edit(CottageTypeAmenities $cottageTypeAmenities)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CottageTypeAmenities  $cottageTypeAmenities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CottageTypeAmenities $cottageTypeAmenities)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CottageTypeAmenities  $cottageTypeAmenities
     * @return \Illuminate\Http\Response
     */
    public function destroy(CottageTypeAmenities $cottageTypeAmenities)
    {
        //
    }
}
