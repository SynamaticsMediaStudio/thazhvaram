<?php

namespace App\Http\Controllers;

use App\Amenities;
use App\CottageType;
use Illuminate\Http\Request;

class CottageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cottageTypes = CottageType::paginate();
        return view('cottages.types.index',['cottageTypes'=>$cottageTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $amenities  =   Amenities::all();
        return view('cottages.types.create',['amenities'=>$amenities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required|string|max:255",
            "rooms" => "required|numeric|min:1",
            "max_children" => "nullable|numeric",
            "max_adults" => "required|numeric|min:1",
            "share_percentage" => "required|numeric|min:1|max:100",
            "cost" => "required|numeric",
            "gst" => "required|numeric|min:1|max:100",
            "amenities" => "nullable|array",
        ]);
        $cottageType = CottageType::create([
            'name'  => $request->name,
            'rooms' => $request->rooms,
            'max_children'  => $request->max_children ?? 0,
            'max_adults'    => $request->max_adults,
            'share_percentage'  => $request->share_percentage,
            'cost'  => $request->cost,
            'gst'   => $request->gst,            
        ]);
        if($cottageType){
            $cottageType->amenities()->sync($request->amenities);
            return redirect()->route('cottage-types.index')->with(['success'=>__('cottage_types.messages.cottage_type_created')]);
        }
        else{
            return redirect()->back()->with(['error'=>__('cottage_types.messages.unable_to_create_cottage_type_created')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CottageType  $cottageType
     * @return \Illuminate\Http\Response
     */
    public function show(CottageType $cottageType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CottageType  $cottageType
     * @return \Illuminate\Http\Response
     */
    public function edit(CottageType $cottageType)
    {
        $amenities  =   Amenities::all();
        return view('cottages.types.edit',['amenities'=>$amenities,'cottageType'=>$cottageType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CottageType  $cottageType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CottageType $cottageType)
    {
        $request->validate([
            "name"=>"required|string|max:255",
            "rooms" => "required|numeric|min:1",
            "max_children" => "nullable|numeric",
            "max_adults" => "required|numeric|min:1",
            "share_percentage" => "required|numeric|min:1|max:100",
            "cost" => "required|numeric",
            "gst" => "required|numeric|min:1|max:100",
            "amenities" => "nullable|array",
        ]);
        $cottageType->update([
            'name'  => $request->name,
            'rooms' => $request->rooms,
            'max_children'  => $request->max_children ?? 0,
            'max_adults'    => $request->max_adults,
            'share_percentage'  => $request->share_percentage,
            'cost'  => $request->cost,
            'gst'   => $request->gst,            
        ]);
        if($cottageType){
            $cottageType->amenities()->sync($request->amenities);
            return redirect()->route('cottage-types.index')->with(['success'=>__('cottage_types.messages.cottage_type_update')]);
        }
        else{
            return redirect()->back()->with(['error'=>__('cottage_types.messages.unable_to_create_cottage_type_update')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CottageType  $cottageType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CottageType $cottageType)
    {
        //
    }
}
