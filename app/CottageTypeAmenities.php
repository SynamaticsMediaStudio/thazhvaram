<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CottageTypeAmenities extends Model
{
    protected $fillable = ['cottage_type_id','amenities_id'];

    //
}
