<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Notifications\UserCreated;
class AdminGen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_existance = User::where('email','synamatics@gmail.com')->first();
        if(!$user_existance){
            $password = sha1(time());
            $user = User::create([
                "name"     => "Admin",
                "email"    => "synamatics@gmail.com",
                'password' => Hash::make($password),
            ]);
            if($user){
                $user->notify(new UserCreated($user,$password));
            }
        }
        return 0;
    }
}
