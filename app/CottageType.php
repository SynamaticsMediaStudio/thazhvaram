<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CottageType extends Model
{
    protected $fillable = ['name','rooms','max_children','max_adults','share_percentage','cost','gst'];
    protected $casts = [
        'name' => 'string',
        'rooms' => 'numeric',
        'max_adults' => 'numeric',
        'max_children' => 'numeric',
        'share_percentage' => 'float',
        'cost' => 'float',
        'gst' => 'float',
    ];
    public function amenities()
    {
        return $this->belongsToMany('App\Amenities','cottage_type_amenities');
    }
}
