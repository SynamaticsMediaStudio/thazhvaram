<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register'=>false]);
// Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::resource('/users', 'UserController')->middleware('auth');
Route::resource('/cottage-types', 'CottageTypeController')->middleware('auth');
Route::resource('/cottages', 'CottageController')->middleware('auth');
Route::resource('/customers', 'CustomerController')->middleware('auth');
Route::resource('/customer-kyc', 'CustomerKycController')->middleware('auth');
Route::resource('/addons', 'AddonController')->middleware('auth');
Route::resource('/bookings', 'BookingController')->middleware('auth');
Route::resource('/transactions', 'TransactionController')->middleware('auth');
Route::resource('/amenities', 'AmenitiesController')->middleware('auth');
