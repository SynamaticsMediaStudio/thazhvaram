@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('cottages.index')}}" class="btn btn-sm btn-light">@lang('general.back')</a>
    </div>
    <h2 class="font-weight-light">@lang('general.update') {{trans_choice('cottages.cottage',1)}} : {{$cottage->building_id}}</h2>
</div>
<form method="POST" action="{{route('cottages.update',$cottage)}}">
    @method('PUT')
    @csrf
<table class="table table-sm table-bordered">
    <tbody>
        <tr>
            <th class="w-25">@lang('cottages.building_id')</th>
            <td>
                <input type="text" name="building_id" id="building_id" class="form-control form-control-sm @error('building_id') is-invalid @enderror" value="{{old('building_id',$cottage->building_id)}}" autocomplete="OFF"/>
                @error('building_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>{{trans_choice('cottage_types.cottage_type',1)}}</th>
            <td>
                <select  name="cottage_type_id" id="cottage_type_id" class="form-control form-control-sm @error('cottage_type_id') is-invalid @enderror" value="{{old('cottage_type_id')}}" autocomplete="OFF">
                    <option selected disabled>--Select Cottage Type--</option>
                    @forelse ($cottageTypes as $cottageType)
                        <option {{old('cottage_type_id',$cottage->cottage_type_id) == $cottageType->id ? "selected":''}} value="{{$cottageType->id}}">{{$cottageType->name}}</option>
                    @empty
                    @endforelse
                </select>
                @error('cottage_type_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
    </tbody>
</table>
<div class="container">
    <div class="float-right">
        <button type="submit" class="btn btn-info">@lang('general.update')</button>
    </div>
</div>
</form>

@endsection
