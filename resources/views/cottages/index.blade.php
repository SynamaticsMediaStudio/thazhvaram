@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('cottages.create')}}" class="btn btn-sm btn-light">@lang('general.add_new') {{trans_choice('cottages.cottage',1)}}</a>
    </div>
    <h2 class="font-weight-light">{{trans_choice('cottages.cottage',2)}}</h2>
</div>
<table class="table table-sm table-bordered text-center">
    <thead>
        <tr>
            <th>@lang('cottages.building_id')</th>
            <th>{{trans_choice('cottage_types.cottage_type',1)}}</th>
            <th>@lang('cottage_types.rooms')</th>
            <th>@lang('cottage_types.max_children')</th>
            <th>@lang('cottage_types.max_adults')</th>
            <th>@lang('cottage_types.share_percentage')</th>
            <th>@lang('cottage_types.cost')</th>
            <th>@lang('cottage_types.gst')</th>
            <th>{{trans_choice('amenities.amenity',2)}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cottages as $cottage)
        <tr>
            <td><a href="{{route('cottages.edit',$cottage)}}">{{$cottage->building_id}}</a></td>
            <td><a href="{{route('cottage-types.edit',$cottage->cottage_type)}}">{{$cottage->cottage_type->name}}</a></td>
            <td>{{$cottage->cottage_type->rooms}}</td>
            <td>{{$cottage->cottage_type->max_children}}</td>
            <td>{{$cottage->cottage_type->max_adults}}</td>
            <td>{{$cottage->cottage_type->share_percentage}}%</td>
            <td>{{$cottage->cottage_type->cost}}</td>
            <td>{{$cottage->cottage_type->gst}}%</td>
            <td>{{$cottage->cottage_type->amenities->pluck('name')->implode(', ')}}</td>
        </tr>
        @empty
            <td colspan="8" class="text-center">@lang('cottages.warnings.no_cottages_added')</td>
        @endforelse
    </tbody>
</table>
{{$cottages->links()}}
@endsection
