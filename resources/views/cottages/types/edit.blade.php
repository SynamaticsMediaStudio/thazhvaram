@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('cottage-types.index')}}" class="btn btn-sm btn-light">@lang('general.back')</a>
    </div>
    <h2 class="font-weight-light">@lang('general.edit') {{trans_choice('cottage_types.cottage_type',1)}} : {{$cottageType->name}}</h2>
</div>
<form method="POST" action="{{route('cottage-types.update',$cottageType)}}">
    @method("PUT")
    @csrf
<table class="table table-sm table-bordered">
    <tbody>
        <tr>
            <th class="w-25">@lang('cottage_types.name')</th>
            <td>
                <input type="text" name="name" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror" value="{{old('name',$cottageType->name)}}" autocomplete="OFF"/>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.rooms')</th>
            <td>
                <input type="number" step="1"  name="rooms" id="rooms" class="form-control form-control-sm @error('rooms') is-invalid @enderror" value="{{old('rooms',$cottageType->rooms)}}" autocomplete="OFF"/>
                @error('rooms')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.max_children')</th>
            <td>
                <input type="number" step="1"  name="max_children" id="max_children" class="form-control form-control-sm @error('max_children') is-invalid @enderror" value="{{old('max_children',$cottageType->max_children)}}" autocomplete="OFF"/>
                @error('max_children')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.max_adults')</th>
            <td>
                <input type="number" step="1"  name="max_adults" id="max_adults" class="form-control form-control-sm @error('max_adults') is-invalid @enderror" value="{{old('max_adults',$cottageType->max_adults)}}" autocomplete="OFF"/>
                @error('max_adults')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.share_percentage')</th>
            <td>
                <input type="number" max="100" step="0.01" name="share_percentage" id="share_percentage" class="form-control form-control-sm @error('share_percentage') is-invalid @enderror" value="{{old('share_percentage',$cottageType->share_percentage)}}" autocomplete="OFF"/>
                @error('share_percentage')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.cost')</th>
            <td>
                <input type="number" step="0.01"  name="cost" id="cost" class="form-control form-control-sm @error('cost') is-invalid @enderror" value="{{old('cost',$cottageType->cost)}}" autocomplete="OFF"/>
                @error('cost')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('cottage_types.gst')</th>
            <td>
                <input type="number" max="100" step="0.01" name="gst" id="gst" class="form-control form-control-sm @error('gst') is-invalid @enderror" value="{{old('gst',$cottageType->gst)}}" autocomplete="OFF"/>
                @error('gst')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
        <tr>
            <th>@lang('amenities.amenity')</th>
            <td>
                @foreach ($amenities as $amenity)
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" {{in_array($amenity->id,old('amenities',$cottageType->amenities->pluck('id')->toArray())) ? "checked":'' }} id="amenity-{{$amenity->id}}" value="{{$amenity->id}}" name="amenities[]">
                    <label class="custom-control-label" for="amenity-{{$amenity->id}}">{{$amenity->name}}</label>
                </div>
                @endforeach
                @error('amenities')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </td>
        </tr>        
    </tbody>
</table>
<div class="container">
    <div class="float-right">
        <button type="submit" class="btn btn-info">@lang('general.update')</button>
    </div>
</div>
</form>

@endsection
