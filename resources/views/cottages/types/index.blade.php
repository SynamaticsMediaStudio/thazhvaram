@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('cottage-types.create')}}" class="btn btn-sm btn-light">@lang('general.add_new') {{trans_choice('cottage_types.cottage_type',1)}}</a>
    </div>
    <h2 class="font-weight-light">{{trans_choice('cottage_types.cottage_types',2)}}</h2>
</div>
<table class="table table-sm table-bordered text-center">
    <thead>
        <tr>
            <th>@lang('cottage_types.name')</th>
            <th>@lang('cottage_types.rooms')</th>
            <th>@lang('cottage_types.max_children')</th>
            <th>@lang('cottage_types.max_adults')</th>
            <th>@lang('cottage_types.share_percentage')</th>
            <th>@lang('cottage_types.cost')</th>
            <th>@lang('cottage_types.gst')</th>
            <th>{{trans_choice('amenities.amenity',2)}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cottageTypes as $cottageType)
        <tr>
            <td><a href="{{route('cottage-types.edit',$cottageType)}}">{{$cottageType->name}}</a></td>
            <td>{{$cottageType->rooms}}</td>
            <td>{{$cottageType->max_children}}</td>
            <td>{{$cottageType->max_adults}}</td>
            <td>{{$cottageType->share_percentage}}%</td>
            <td>{{$cottageType->cost}}</td>
            <td>{{$cottageType->gst}}%</td>
            <td>{{$cottageType->amenities->pluck('name')->implode(', ')}}</td>
        </tr>
        @empty
            <td colspan="8" class="text-center">@lang('cottage_types.warnings.no_cottage_types_added')</td>
        @endforelse
    </tbody>
</table>
{{$cottageTypes->links()}}
@endsection
