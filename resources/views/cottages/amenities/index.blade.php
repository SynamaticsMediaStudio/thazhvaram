@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('amenities.create')}}" class="btn btn-sm btn-light">@lang('general.add_new') {{trans_choice('amenities.amenity',1)}}</a>
    </div>
    <h2 class="font-weight-light">{{trans_choice('amenities.amenity',2)}}</h2>
</div>
<table class="table table-sm table-bordered text-center">
    <thead>
        <tr>
            <th>@lang('amenities.name')</th>
            <th>{{trans_choice('cottage_types.cottage_type',2 )}}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($amenities as $amenity)
        <tr>
            <td><a href="{{route('amenities.edit',$amenity)}}">{{$amenity->name}}</a></td>
            <td>{{$amenity->cottage_types->pluck('name')->implode(',')}}</td>
        </tr>
        @empty
            <td colspan="8" class="text-center">@lang('amenities.warnings.no_amenities_added')</td>
        @endforelse
    </tbody>
</table>
{{$amenities->links()}}
@endsection
