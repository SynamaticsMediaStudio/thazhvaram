@extends('layouts.app')
@section('content')
<div class="py-4 px-4 text-light bg-primary">
    <div class="float-right">
        <a href="{{route('amenities.index')}}" class="btn btn-sm btn-light">@lang('general.back')</a>
    </div>
    <h2 class="font-weight-light">@lang('general.add_new') {{trans_choice('amenities.amenity',1)}}</h2>
</div>
<form method="POST" action="{{route('amenities.store')}}">
    @csrf
<table class="table table-sm table-bordered">
    <tbody>
        <tr>
            <th class="w-25">@lang('amenities.name')</th>
            <td>
                <input type="text" name="name" id="name" class="form-control form-control-sm @error('name') is-invalid @enderror" value="{{old('name')}}" autocomplete="OFF"/>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
            <th>@lang('amenities.description')</th>
            <td>
                <textarea name="description" id="description" class="form-control form-control-sm @error('description') is-invalid @enderror" value="{{old('description')}}" autocomplete="OFF" rows="10"></textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror                
            </td>
        </tr>
    </tbody>
</table>
<div class="container">
    <div class="float-right">
        <button type="submit" class="btn btn-success">@lang('general.save')</button>
    </div>
</div>
</form>

@endsection
