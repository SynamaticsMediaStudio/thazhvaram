<li class="nav-item dropdown">
    <a id="cottageDropDown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Cottages
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="cottageDropDown">
        <a class="dropdown-item" href="{{ route('cottages.index') }}">All Cottages</a>
        <a class="dropdown-item" href="{{ route('cottages.create') }}">Add Cottage</a>
        <a class="dropdown-item" href="{{ route('cottage-types.index') }}">Cottage Types</a>
        <a class="dropdown-item" href="{{ route('cottage-types.create') }}">Add Cottage Types</a>
        <a class="dropdown-item" href="{{ route('amenities.index') }}">Amenities</a>
        <a class="dropdown-item" href="{{ route('amenities.create') }}">Add Amenity</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a id="usersDropDown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Users
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="usersDropDown">
        <a class="dropdown-item" href="{{ route('users.index') }}">All Users</a>
        <a class="dropdown-item" href="{{ route('users.create') }}">Add User</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a id="customersDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Customers
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customersDropdown">
        <a class="dropdown-item" href="{{ route('customers.index') }}">All Customers</a>
        <a class="dropdown-item" href="{{ route('customers.create') }}">Add Customer</a>
        <a class="dropdown-item" href="{{ route('customer-kyc.index') }}">Kyc Documents</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a id="bookingsDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        Bookings
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bookingsDropdown">
        <a class="dropdown-item" href="{{ route('bookings.index') }}">All Bookings</a>
        <a class="dropdown-item" href="{{ route('bookings.create') }}">Add Booking</a>
        <a class="dropdown-item" href="{{ route('transactions.index') }}">Transactions</a>
        <a class="dropdown-item" href="{{ route('transactions.create') }}">Add Transaction</a>
    </div>
</li>