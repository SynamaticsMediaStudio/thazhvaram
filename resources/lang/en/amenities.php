<?php

return [
    "amenity"  => "Amenity|Amenities",
    "name"  => "Name",
    "description" => "Description", 
    "icon" => "Icon", 
    "warnings"   => [
        "no_amenities_added"=>"No amenities added"
    ],
    "messages"   => [
        "created"=>"New amenity created",
        "unable_to_create"=>"Unable to create amenity",
        "updated"=>"Amenity Updated",
        "unable_to_update"=>"Unable to update amenity",
    ],
];
