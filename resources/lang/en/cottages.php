<?php

return [
    "cottage"  => "Cottage|Cottages",
    "building_id"  => "Building ID",
    "cottage_type" => "Cottage Type", 
    "warnings"   => [
        "no_cottages_added"=>"No Cottages added"
    ],
    "messages"   => [
        "cottage_type_created"=>"New Cottage Created",
        "unable_to_create_cottage_type_created"=>"Unable to create cottage",
        "cottage_type_update"=>"Cottage Updated",
        "unable_to_create_cottage_type_update"=>"Unable to update cottage",
    ],
];
