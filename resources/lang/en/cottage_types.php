<?php

return [
    "cottage_type"  => "Cottage Type|Cottage Types",
    "cottage_types"  => "Cottage Types",
    "name"  => "Name",
    "rooms" => "Rooms",
    "max_children"  => "Maximum Children",
    "max_adults"    => "Maximum Adults",
    "share_percentage"  => "Share Percentage",
    "cost"  => "Cost",
    "gst"   => "GST (Percentage)",    
    "warnings"   => [
        "no_cottage_types_added"=>"No Cottage Types added"
    ],
    "messages"   => [
        "cottage_type_created"=>"New Cottage Type Created",
        "unable_to_create_cottage_type_created"=>"Unable to create cottage type",
        "cottage_type_update"=>"Cottage Type Updated",
        "unable_to_create_cottage_type_update"=>"Unable to update cottage type",
    ],
];
