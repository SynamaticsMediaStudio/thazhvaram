<?php

return [
    "add_new"  => "Add New",
    "back"  => "Back",
    "save"  => "Save",
    "edit"  => "Edit",
    "update"  => "Update",
    "messages"   => [
        "created"=>"New :type created",
        "unable_to_create"=>"Unable to create :type",
        "updated"=>":type Updated",
        "unable_to_update"=>"Unable to update :type",
    ],    
];
